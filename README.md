***
```
Установка по прямой ссылке
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \
  && chmod +x minikube

sudo mkdir -p /usr/local/bin/
sudo install minikube /usr/local/bin/
```
***
```
minikube start --vm-driver=<driver_name>
minikube start --vm-driver=docker
```
***
```
minikube stop
```
***
```
minikube delete
```
***
```
Статус cpu: <unknown>/80% в HorizontalPodAutoscaler (HPA) указывает на то, что Kubernetes не может получить данные о метриках CPU. Это может быть вызвано несколькими причинами:

Метрики не собираются: Убедитесь, что компонент Kubernetes Metrics Server установлен и работает корректно в вашем кластере. Metrics Server собирает данные о ресурсах и предоставляет их HPA.

Вы можете проверить статус Metrics Server с помощью команды:

sh
Копировать код
kubectl get deployment metrics-server -n kube-system
Если Metrics Server не установлен, вы можете установить его, выполнив следующие команды:

sh
Копировать код
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
Проблемы с разрешениями: Убедитесь, что HPA имеет необходимые разрешения для чтения метрик. Обычно это означает, что у роли, привязанной к сервисному аккаунту, который использует HPA, должны быть необходимые разрешения для чтения метрик из API.

Недостаток времени: В некоторых случаях требуется немного времени, чтобы метрики начали поступать после установки HPA. Подождите несколько минут и проверьте снова.

Некорректные конфигурации: Убедитесь, что конфигурация вашего HPA правильная и соответствует стандартам Kubernetes.

Чтобы проверить состояние Metrics Server и узнать, собираются ли метрики, выполните команду:

sh
Копировать код
kubectl top pods
Эта команда должна отобразить текущую нагрузку на CPU и память для каждого пода. Если вы видите данные о метриках, это значит, что Metrics Server работает корректно.

Если данные не отображаются, попробуйте выполнить команду для диагностики проблем с Metrics Server:

sh
Копировать код
kubectl logs -n kube-system deployment/metrics-server
Эта команда покажет журналы работы Metrics Server, где можно найти возможные ошибки и проблемы.
```
```
***
Ошибка, которая появляется в логах Metrics Server, указывает на проблемы с проверкой сертификата TLS для node minikube. Эта проблема возникает, потому что сертификат не содержит IP SAN (Subject Alternative Name), необходимый для проверки подлинности IP-адреса.

Есть несколько способов решения этой проблемы:

Способ 1: Обновление параметров Metrics Server
Попробуйте переустановить Metrics Server с настройками, которые отключают проверку сертификатов. Это можно сделать, добавив следующие аргументы при установке Metrics Server:

Скачайте YAML-файл для Metrics Server и отредактируйте его:

sh
Копировать код
curl -O https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
В открытом файле найдите Deployment для Metrics Server и добавьте аргументы --kubelet-insecure-tls и --kubelet-preferred-address-types=InternalIP:

yaml
Копировать код
spec:
  containers:
  - args:
    - --cert-dir=/tmp
    - --secure-port=10250
    - --kubelet-preferred-address-types=InternalIP,Hostname,InternalDNS,ExternalDNS,ExternalIP
    - --kubelet-insecure-tls
    image: k8s.gcr.io/metrics-server/metrics-server:v0.6.1
    name: metrics-server
Примените изменения:

sh
Копировать код
kubectl apply -f components.yaml
```
***





